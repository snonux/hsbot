all:
	ghc --make Main.hs -o hsbot
ghci:
	ghci Main.hs 
clean:
	find . -name \*.o | xargs rm
	find . -name \*.hi | xargs rm
	rm hsbot

