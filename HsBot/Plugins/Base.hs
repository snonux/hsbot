module HsBot.Plugins.Base where

import HsBot.Base.Env
import HsBot.Base.State

type CallbackFunction = String -> SendMessage -> Env -> IO Env

data Plugin a = Plugin {
   cbPrio :: Integer,
   cbFunction :: CallbackFunction
}

