module HsBot.Plugins.MessageCounter (makeMessageCounter) where

import HsBot.Plugins.Base

import HsBot.Base.Env
import HsBot.Base.State

import HsBot.IRC.User

update user = user { userMessages = 1 + userMessages user }

messageCounter :: CallbackFunction
messageCounter str sendMessage (Env state conf) = do
   return (Env (stateUpdateUser state (currentSender state) update) conf)

makeMessageCounter = Plugin 0 messageCounter
