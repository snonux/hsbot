module HsBot.Plugins.Dummy (makeDummy) where

import HsBot.Plugins.Base

import HsBot.Base.Env
import HsBot.Base.State

dummy :: CallbackFunction
dummy str sendMessage env@(Env state _) = return (env)

makeDummy = Plugin 0 dummy
