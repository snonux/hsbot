module HsBot.Plugins.PrintMessages (makePrintMessages) where

import HsBot.Plugins.Base

import HsBot.Base.Env
import HsBot.Base.State

printMessages :: CallbackFunction
printMessages str sendMessage env@(Env state _) = do
   putStrLn $ (currentChannel state) ++ " "
      ++ (currentSender state) ++ ": "
      ++ str
   return (env)

makePrintMessages = Plugin 0 printMessages
