module HsBot.Plugins.StoreMessages (makeStoreMessages) where

import Control.Exception

import HsBot.Plugins.Base

import HsBot.Base.Env
import HsBot.Base.State

storeMessages :: CallbackFunction
storeMessages str sendMessage env@(Env state _) = do
	return (env)

makeStoreMessages = Plugin 0 storeMessages

