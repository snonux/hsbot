module HsBot.Plugins (pluginsTrigger) where

import HsBot.Base.Env
import HsBot.Base.State

import HsBot.Plugins.Base
import HsBot.Plugins.MessageCounter
import HsBot.Plugins.PrintMessages
import HsBot.Plugins.StoreMessages
import HsBot.Plugins.Dummy

registeredPlugins = [
  makeMessageCounter,
  makePrintMessages, 
  makeStoreMessages, 
  makeDummy
  ]

pluginsTrigger :: String -> SendMessage -> Env -> IO Env
pluginsTrigger str sendMessage env@(Env state _) 
      | isReady state = pluginsAll registeredPlugins env
      | otherwise = do { putStrLn str; return (env) }
   where
      pluginsAll [] env = return (env)
      pluginsAll (plugin:restPlugins) env = do
      	env' <- (cbFunction plugin) str sendMessage env
      	pluginsAll restPlugins env'

