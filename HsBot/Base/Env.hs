module HsBot.Base.Env where

import HsBot.Base.Conf
import HsBot.Base.State

type SendMessage = String -> IO ()
type DispatchFunction = String -> SendMessage -> Env -> IO Env

data Env = DispatchEnv State Conf DispatchFunction | Env State Conf

castEnv :: Env -> Env
castEnv (DispatchEnv state conf _) = Env state conf

envGetInt :: String -> Env -> Int
envGetInt key (Env _ conf) = getUnwrappedInt key conf
envGetInt key env = envGetInt key (castEnv env)

envGet :: String -> Env -> String
envGet key (Env _ conf) = getUnwrapped key conf
envGet key env = envGet key (castEnv env)

envState :: Env -> State
envState (Env state _) = state
envState env = envState (castEnv env)
