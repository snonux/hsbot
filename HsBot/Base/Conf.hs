module HsBot.Base.Conf where

import qualified Data.Map as M

type Conf = M.Map String String

makeConf = M.fromList 
         [ ("name", "HsBot")
         , ("version", "v0.0")
         , ("databaseFile", "hsbot.db")
         , ("maxMessageSize", "400")
         , ("admin", "rantanplan")
         , ("ircServer", "irc.german-elite.net")
         , ("ircChannel", "#buetow.org")
         , ("ircNick", "hotdog")
         , ("ircPort", "6667")
         , ("ircUser", "hsbot.buetow.org")
         , ("dbHost", "localhost")
         , ("dbUser", "hsbot")
         , ("dbPass", "hsbot")
         , ("dbSchema", "hsbot")
         ]

get :: (Monad m) => String -> Conf -> m String
get key conf = case M.lookup key conf of
	Just val -> return (val)
	Nothing -> return ("")
	
getUnwrappedInt :: String -> Conf -> Int
getUnwrappedInt key conf = read (getUnwrapped key conf) :: Int

getUnwrapped :: String -> Conf -> String
getUnwrapped key conf = 
	case M.lookup key conf of
		Just val -> val
		Nothing -> ""
