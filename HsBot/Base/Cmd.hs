module HsBot.Base.Cmd where

import HsBot.Base.State

data Cmd = Cmd String String (State -> IO ()) 

instance Show Cmd where 
   show (Cmd a b _) = a ++ " - " ++ b

cmdGet :: String -> [Cmd] -> Maybe Cmd
cmdGet x commands = 
   let command = [ (Cmd a b c) | (Cmd a b c) <- commands, a == x ]
    in if length command == 0
         then Nothing
         else Just (head command)

