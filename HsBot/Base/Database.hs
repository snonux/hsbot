module HsBot.Base.Database where

import HsBot.Base.Conf

import qualified Database.HSQL.MySQL as MySQL
import qualified Database.HSQL.Types as Types

data Database = Database {
		connection :: IO Types.Connection,
		connectionString :: String
	} 

instance Show Database where
	show database = show $ connectionString database

databaseMake :: Conf -> IO Database
databaseMake conf = do
	dbHost <- get "dbHost" conf
	dbUser <- get "dbUser" conf
	dbSchema <- get "dbSchema" conf
	dbPass <- get "dbPass" conf
	return $ Database { 
		connection = MySQL.connect dbHost dbUser dbSchema dbPass,
		connectionString = dbHost ++ " " ++ dbUser ++ " " ++ dbSchema ++ " " ++ dbPass
	}

