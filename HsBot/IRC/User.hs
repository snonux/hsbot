module HsBot.IRC.User where

import List

import HsBot.General.Render

data User = User { 
      userName :: String,
      userMessages :: Int,
      userPts :: Int 
   } deriving (Show, Read) 

userMakeDefault :: String -> User
userMakeDefault name = 
	User { 
		userName = name, 
		userMessages = 0,
		userPts = 0
	}

instance Eq User where 
   x == y = (userPts x) == (userPts y)

instance Ord User where
	x < y = (userPts x) < (userPts y)
	x > y = (userPts x) > (userPts y)
	x >= y = not (x < y)
	x <= y = not (x > y)

instance Render User where
	render user = userName user ++ ": " ++ 
		(show $ userPts user) ++ "pts; " ++
		(show $ userMessages user) ++ "msgs"

userEquals :: User -> User -> Bool
userEquals x y = (userName x) == (userName y)

userGetIfExists :: String -> [User] -> Maybe User
userGetIfExists name [] = Nothing
userGetIfExists name (x:xs) 
	| userName x == name = Just x
	| otherwise = userGetIfExists name xs

userDeleteIfExists :: String -> [User] -> [User]
userDeleteIfExists name [] = []
userDeleteIfExists name (x:xs) 
	| userName x == name = xs
	| otherwise = x : userDeleteIfExists name xs

userGet :: String -> [User] -> (User, [User])
userGet name xs =
	case userGetIfExists name xs of
		Just user -> (user, user : xs)
		Nothing -> let user = userMakeDefault name in (user, user : xs)

usersUpdate :: String -> [User] -> (User -> User) -> [User]
usersUpdate name xs update = 
   let (user, _) = userGet name xs
    in (update user) : (userDeleteIfExists name xs)
