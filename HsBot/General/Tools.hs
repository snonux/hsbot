module HsBot.General.Tools where

uniq :: (Eq a) => [a] -> [a]
uniq list = 
	let r = u' list 0                    
            u' [] _ = []              
  	    u' (x:list) n 
		   | member x r n = u' list n 
		   | otherwise = x:(u' list (n + 1))
  	    member e list 0 = False
  	    member y (x:list) n = x == y || member y list (n - 1)
    in r

split :: String -> Char -> [String]
split [] delim = [""]
split (c:cs) delim 
      | c == delim = "" : rest
   	| otherwise = (c : head rest) : tail rest
   where rest = split cs delim

empty :: String -> Bool
empty str = length str == 0

contains :: String -> Char -> Bool
contains str char = takeWhile (/= char) str /= str

isMultiline :: String -> Bool
isMultiline str = contains str '\n'

showL :: Show a => [a] -> [String]
showL = map (\x -> show x ++ "\n")

